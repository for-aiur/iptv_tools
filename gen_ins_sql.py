import os
import csv
import argparse

SUPPORTED_IPTV_FORMATS = ['.m3u', '.m3u8']

parser = argparse.ArgumentParser()
parser.add_argument('-c', '--csv', required=True)
parser.add_argument('-f', '--folder', required=True)
parser.add_argument('-p', '--provider_ref', default='data/iptv_providers.csv')
parser.add_argument('-t', '--tv_ref', default='data/tv_channels.csv')
args = parser.parse_args()


if not os.path.exists(args.csv):
    raise IOError('file does not exist')
csv_path = args.csv

if not os.path.exists(args.folder):
    raise IOError('iptv folder does not exist')
iptv_folder = args.folder

if not os.path.exists(args.provider_ref):
    raise IOError('iptv providers ref file does not exist')
provider_ref_path = args.provider_ref

if not os.path.exists(args.tv_ref):
    raise IOError('tv channels ref file does not exist')
tv_channels_ref_path = args.tv_ref
    
insert_template = 'insert into iptv_links values (NULL, "{}", {}, {});'


def get_id_of_channel(channel_name):
    with open(tv_channels_ref_path) as f:
        csv_reader = csv.reader(f, delimiter=',')
        for row in csv_reader:
            channel_id = row[0]
            channel_name_ref = row[1]
            if channel_name_ref == channel_name:
                return channel_id
    raise ValueError('channel {} has no id, stopped'.format(channel_name)) 


def get_id_of_provider(provider_name):
    with open(provider_ref_path) as f:
        csv_reader = csv.reader(f, delimiter=',')
        for row in csv_reader:
            provider_id = row[0]
            provider_name_ref = row[1]
            if provider_name_ref == provider_name:
                return provider_id
    raise ValueError('provider {} has no id, stopped'.format(provider_name)) 


def get_link_from_iptv_file(iptv_file_path, search_string):
    with open(iptv_file_path) as f:
        lines = f.readlines()
        for i in range(0, len(lines)):
            line = lines[i].rstrip()
            if line == search_string:
                return lines[i+1]
    raise ValueError('{} does not have {}, stopped'.format(iptv_file_path, search_string))


def write_sql_to_file(sql_list, out_filename):
    with open(out_filename, "w") as f:
        for sql_line in sql_list:
            f.write(sql_line)
            f.write('\n')


if __name__ == '__main__':
    sql_list = []

    # read input csv with tv link header, indicator and channel name
    with open(csv_path, 'r') as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
        for row in csv_reader:
            ss = row[0]
            channel_name = row[1]
            
            # loop through folders
            for f in os.listdir(iptv_folder):
                file_path=os.path.join(iptv_folder, f)
                provider_name = os.path.splitext(f)[0]
                ext = os.path.splitext(f)[1]

                if ext not in SUPPORTED_IPTV_FORMATS:
                    continue

                channel_id = get_id_of_channel(channel_name)
                provider_id = get_id_of_provider(provider_name)
                
                link = get_link_from_iptv_file(file_path, ss)
                
                insert_sql = insert_template.format(link, provider_id, channel_id)
                sql_list.append(insert_sql)
                
    outfilename = os.path.basename(iptv_folder) + '.sql'
    print('Following list of sql statements are written to file \r\n\r\n {}: \r\n\r\n look at {}'.format(sql_list, outfilename))
    write_sql_to_file(sql_list, outfilename)
